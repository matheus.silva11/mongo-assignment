package com.avenuecode.nosql.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.avenuecode.nosql.models.Usuario;

public interface UsuarioRepository extends MongoRepository<Usuario, String> {

	public Usuario findByFirstName(String firstName);

	public List<Usuario> findByLastName(String lastName);

}