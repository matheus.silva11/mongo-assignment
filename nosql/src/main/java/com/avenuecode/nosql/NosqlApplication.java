package com.avenuecode.nosql;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

import com.avenuecode.nosql.models.Usuario;
import com.avenuecode.nosql.repositories.UsuarioRepository;

@SpringBootApplication
@EnableCaching
public class NosqlApplication implements CommandLineRunner{
	
	@Autowired
	UsuarioRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(NosqlApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		
		Usuario usuariobase = new Usuario();
		// limpando base
		repository.deleteAll();
		
		// Adicionando alguns usuarios
	    repository.save(new Usuario("Matheus", "Silva", "123456"));
	    repository.save(new Usuario("Augusto", "Silva", "456123"));
	    repository.save(new Usuario("Leticia", "Leite", "987654"));
	    
	    // buscando usuarios cadastrados
	    List<Usuario> usuarios = repository.findAll();
	    System.out.println("Usuarios Cadastrados: ");
	    for (Usuario usuario : usuarios) {
			System.out.println("Nome: "+usuario.getFirstName() + " - Documento: "+usuario.getDocument());
		}
	    
	    System.out.println();
	    System.out.println();
	    
	    // atualizando alguns usuarios
	    usuariobase = repository.findByFirstName("Matheus");
	    System.out.println("Atualizando : "+usuariobase.getFirstName());
	    usuariobase.setDocument("abcde");
	    repository.save(usuariobase);
	    System.out.println("Novo documento para : "+usuariobase.getDocument());
	    
	    usuariobase = repository.findByFirstName("Augusto");
	    System.out.println("Atualizando : "+usuariobase.getFirstName());
	    usuariobase.setDocument("fghi");
	    repository.save(usuariobase);
	    System.out.println("Novo documento para : fghi");
	    
	    System.out.println();
	    System.out.println();
	    
	    // deletando usuario
	    usuariobase = repository.findByFirstName("Leticia");
	    System.out.println("Deletando usuario: "+usuariobase.getFirstName());
	    repository.delete(usuariobase);
	    usuariobase = repository.findByFirstName("Augusto");
	    System.out.println("Deletando usuario: "+usuariobase.getFirstName());
	    repository.delete(usuariobase);
	    
	    System.out.println();
	    System.out.println();
	    
	    // buscando usuarios cadastrados
	    usuarios = repository.findAll();
	    System.out.println("Usuarios Cadastrados: ");
	    for (Usuario usuario : usuarios) {
			System.out.println("Nome: "+usuario.getFirstName() + " - Documento: "+usuario.getDocument());
		}
	    
	    
	    
		
	}

}
